﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace StrongBackup.App.Attributes
{
    [AttributeUsage(AttributeTargets.Field)
]
    public class EnumValueAttribute : Attribute
    {
        private readonly string _value;

        public EnumValueAttribute(string value)
        {
            _value = value;
        }

        public string EnumValue
        {
            get
            {
                return _value;
            }
        }

    }

    public static class EnumExtensions
    {
        public static string GetEnumValue(this Enum e)
        {
            var type = e.GetType();
            var memInfo = type.GetMember(e.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(EnumValueAttribute), false);
            if (attributes == null || attributes.Length == 0)
            {
                return null;
            }

            var enumValue = ((EnumValueAttribute)attributes[0]).EnumValue;

            return enumValue;
        }
    }
}
