﻿using StrongBackup.App.Attributes;
using StrongBackup.App.Extensions;
using StrongBackup.App.Interfaces;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StrongBackup.App
{
    public class Backup
    {
        private readonly string _source;
        private readonly string _dest;
        private readonly IO _io;
        private readonly ConcurrentBag<UpdateJob> _updates = new ConcurrentBag<UpdateJob>();

        private enum FileType
        {
            File,
            Directory
        }

        private enum FileOp
        {
            [EnumValue("ADD")]
            Add,
            Modify,
            Delete
        }

        private class UpdateJob
        {
            public string Source { get; set; }
            public string Dest { get; set; }
            public FileType FileType { get; set; }
            public FileOp Op { get; set; }
            public IO Io { get;  set; }

            private ConsoleColor Color()
            {
                switch(Op)
                {
                    case FileOp.Add:
                        return ConsoleColor.Green;
                    case FileOp.Modify:
                        return ConsoleColor.Yellow;
                    case FileOp.Delete:
                        return ConsoleColor.Red;
                }
                return ConsoleColor.Red;
            }

            public void Out()
            {
                Io.Out($"{Op.ToString()}: {Source ?? "[DELETED]"} -> {Dest ?? "[DELETED]"}", Color());
            }
            public string Report()
            {
                return $"{Op.ToString()},{Source ?? "[DELETED]"},{Dest ?? "[DELETED]"}";
            }
        }

        public Backup(string sourceDirectory, string destinationDirectory, IO io)
        {

            if (io == null)
            {
                throw new Exception("IO should not be null");
            }
            if (!IsValid(sourceDirectory) || !IsValid(destinationDirectory))
            {
                io.Verbose($"Source: {sourceDirectory ?? "<NULL>"}");
                io.Verbose($"Dest: {destinationDirectory ?? "<NULL>"}");
                throw new Exception("Source or destination is invalid");
            }


            io.Out($"SOURCE: {sourceDirectory}", ConsoleColor.Green);
            io.Out($"DESTINATION: {destinationDirectory}", ConsoleColor.Red);
            io.Out("----------------------------------------", ConsoleColor.White);
            io.Out();
            _source = sourceDirectory;
            _dest = destinationDirectory;
            _io = io;
        }

        private bool IsValid(string directory)
        {
            return !String.IsNullOrWhiteSpace(directory) && Directory.Exists(directory);
        }

        public async Task Diff()
        {
            DirectoryInfo sourceDir = new DirectoryInfo(_source);
            DirectoryInfo destDir = new DirectoryInfo(_dest);
            await DiffDirectory(sourceDir, destDir);
        }

        private async Task DiffDirectory(DirectoryInfo source, DirectoryInfo dest)
        {
            await Task.Run(() =>
            {
                var lookupDictionary = dest.EnumerateFiles().ToConcurrentDictionary(k => k.Name, v => v);
                var touched = new ConcurrentBag<string>();
                source.EnumerateFiles().AsParallel().ForAll(k =>
                {

                    if (lookupDictionary.ContainsKey(k.Name))
                    {
                    //Both files exist.
                    bool isSame = DiffFile(k, lookupDictionary[k.Name]);
                        if (!isSame)
                        {
                            MarkFileModify(k.FullName, lookupDictionary[k.Name].FullName, FileOp.Modify, FileType.File);
                        }
                        else if (_io.VerboseActive())
                        {
                            _io.Verbose($"File from {source.FullName} to {dest.FullName} requires no change");
                        }
                    }
                    else
                    {
                    //Add
                        MarkFileModify(k.FullName, Path.Combine(dest.FullName, k.Name), FileOp.Add, FileType.File);
                    }
                    touched.Add(k.Name);
                });
                dest.EnumerateFiles().AsParallel().ForAll(k =>
                {
                    if (!touched.Contains(k.Name))
                    {
                    //Delete
                    MarkFileModify(null, k.FullName, FileOp.Delete, FileType.File);
                    }
                });
            });

            var directoriesToMarch = new List<Tuple<DirectoryInfo, DirectoryInfo>>();
            var destDirectories = dest.EnumerateDirectories().ToConcurrentDictionary(k => k.Name, v => v);
            var touchedDirs = new ConcurrentBag<string>();
            source.EnumerateDirectories().AsParallel().ForAll(k =>
            {
                if (destDirectories.ContainsKey(k.Name))
                {
                    //Modify
                    directoriesToMarch.Add(new Tuple<DirectoryInfo, DirectoryInfo>(k, destDirectories[k.Name]));
                }
                else
                {
                    //Add
                    MarkFileModify(k.FullName, Path.Combine(dest.FullName, k.Name), FileOp.Add, FileType.Directory);
                }
                touchedDirs.Add(k.Name);
            });
            dest.EnumerateDirectories().ForEach(k =>
            {
                if(!touchedDirs.Contains(k.Name))
                {
                    MarkFileModify(null, k.FullName, FileOp.Delete, FileType.Directory);
                }
            });
            var tasksToWait = new List<Task>();
            directoriesToMarch.ForEach(k =>
            {
                tasksToWait.Add(DiffDirectory(k.Item1, k.Item2));
            });
            await Task.WhenAll(tasksToWait);
        }
        
        private bool DiffFile(FileInfo source, FileInfo dest)
        {
            try
            {
                byte[] sourceHash;
                byte[] destHash;
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(source.FullName))
                    {
                        sourceHash = md5.ComputeHash(stream);
                    }
                    using (var stream = File.OpenRead(dest.FullName))
                    {
                        destHash = md5.ComputeHash(stream);
                    }
                }
                bool areEqual = StructuralComparisons.StructuralEqualityComparer.Equals(sourceHash, destHash);
                if (_io.VerboseActive())
                {
                    _io.Verbose($"Comparison: Source ({source.FullName})[{sourceHash.ToHex()}] | Destination ({dest.FullName})[{destHash.ToHex()}]");
                }
                return areEqual;
            }
            catch(Exception exception)
            {
                _io.Out($"Could not access file {source.FullName}, please copy manually", ConsoleColor.DarkRed);
                return true; //Marks it as not copyable
            }
        }

        private void MarkFileModify(string sourcePath, string destPath, FileOp op, FileType fileType)
        {
            var modify = new UpdateJob
            {
                Source = sourcePath,
                Dest = destPath,
                FileType = fileType,
                Op = op,
                Io = _io
            };
            modify.Out();
            _updates.Add(modify);
        }

        public async Task RunBackup()
        {
            _io.Out();
            _io.Out("Beginning Backup....");
            _io.Out("-------");
            if (_updates == null || _updates.IsEmpty)
            {
                _io.Out("There is nothing to process! (Did you run a diff?)", ConsoleColor.Yellow);
            }
            
            await Task.WhenAll(_updates.Select(k => BackupItem(k)).ToArray());
            _io.Out("------------------------");
            _io.Out("Backup complete!");
            _io.Out("------------------------");
        }

        private async Task BackupItem(UpdateJob job)
        {
            await Task.Run(() =>
            {
                try
                {
                    if (job.FileType == FileType.File)
                    {
                        if (job.Op == FileOp.Add)
                        {
                            File.Copy(job.Source, job.Dest, false);
                            _io.Verbose($"Added {job.Source} -> {job.Dest}");
                        }
                        else if (job.Op == FileOp.Modify)
                        {
                            File.Copy(job.Source, job.Dest, true);
                            _io.Verbose($"Updated {job.Source} -> {job.Dest}");
                        }
                        else if (job.Op == FileOp.Delete)
                        {
                            File.Delete(job.Dest);
                            _io.Verbose($"Deleted {job.Dest}");
                        }
                    }
                    if (job.FileType == FileType.Directory)
                    {
                        if (job.Op == FileOp.Add)
                        {
                            DirectoryCopy(job.Source, job.Dest, true, false);
                        }
                        else if (job.Op == FileOp.Modify)
                        {
                            DirectoryCopy(job.Source, job.Dest, true, true);
                        }
                        else if (job.Op == FileOp.Delete)
                        {
                            Directory.Delete(job.Dest, true);
                        }
                    }
                } catch (Exception exception)
                {
                    _io.Out($"Could not execute {job.Source ?? job.Dest}, please copy manually.", ConsoleColor.DarkRed);
                }
            });
        }

        public List<string> Report()
        {
            return _updates.Select(k => k.Report()).ToList();
        }

        private void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs = true, bool overwrite = false)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            _io.Verbose($"Copying source directory {sourceDirName}");

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                try
                {
                    _io.Verbose($"\tCopying {file.Name}");
                    string temppath = Path.Combine(destDirName, file.Name);
                    file.CopyTo(temppath, overwrite);
                } catch (Exception exception)
                {
                    _io.Out($"Copy of file {file.Name} failed, please transfer manually.", ConsoleColor.DarkRed);
                }
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
}
