﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace StrongBackup.App.Extensions
{
    public static class ConcurrentExtensions
    {
        public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            foreach(var item in source)
            {
                action(item);
            }
        }


        public static ConcurrentDictionary<TKey, TValue> ToConcurrentDictionary<TSource, TKey, TValue>(this IEnumerable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TSource, TValue>> valueSelector, Expression<Func<TValue, TValue, TValue>> duplicateResolver = null)
        {
            var ks = keySelector.Compile();
            var vs = valueSelector.Compile();
            var ds = duplicateResolver?.Compile();
            var dict = new ConcurrentDictionary<TKey, TValue>();
            source.ForEach(k =>
            {
                var key = ks.Invoke(k);
                var value = vs.Invoke(k);

                if (dict.ContainsKey(key))
                {
                    if (ds == null)
                    {
                        throw new Exception($"Key {key} already exists in dictionary. (You can use the merge selector if needed)");
                    }
                    else
                    {
                        var existing = dict[key];
                        dict[key] = ds.Invoke(existing, value);
                    }
                }
                else
                {
                    dict[key] = value;
                }
            });
            return dict;
        }
    }
}
