﻿using System;

namespace StrongBackup.App.Interfaces
{
    public interface IO
    {
        void Out(string text = null, ConsoleColor color = ConsoleColor.White);
        bool VerboseActive();
        void Verbose(string text);
        string In();

    }
}
