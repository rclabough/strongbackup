﻿using StrongBackup.App;
using StrongBackup.App.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace StrongBackup
{
    class Program
    {
        private static string[] _flags;

        static async Task Main(string[] args)
        {
            try
            {
                Console.WriteLine("STRONG BACKUP");
                Console.WriteLine("-------------");

                if (args == null || args.Length < 2)
                {
                    Help();
                    return;
                }

                var source = args[0];
                var dest = args[1];

                _flags = args.Where(k => k.StartsWith('-')).Select(k => k.Substring(1)).ToArray();

                var backup = new Backup(source, dest, new LocalIo());
                await backup.Diff();

                if(_flags.Contains("r"))
                {
                    Report(backup);
                    return;
                }

                Console.WriteLine("Ready to start transfer...");
                Console.ReadLine();
                await backup.RunBackup();
                Console.ReadLine();
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }


        static void Help()
        {
            string appname = "strongbackup";
            Console.WriteLine($"{appname} [Source Directory] [Destination Directory] flags...");
            Console.WriteLine("\t-v\tVerbose logging turned on");
            Console.WriteLine("\t-r\tReport-only mode.  Only log potential changes.");
            return;
        }

        static void Report(Backup backup)
        {
            using (var sw = new StreamWriter("report.csv"))
            {
                foreach(var line in backup.Report())
                {
                    sw.WriteLine(line);
                }
            }
            Console.WriteLine("Saved under report.csv");
        }

        class LocalIo : IO
        {
            public string In()
            {
                lock (this)
                {
                    return Console.ReadLine();
                }
            }

            public void Out(string text = null, ConsoleColor color = ConsoleColor.White)
            {
                lock (this)
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = color;
                    Console.WriteLine(text ?? string.Empty);
                }
            }

            public void Verbose(string text)
            {
                if (VerboseActive())
                {
                    Out(text, ConsoleColor.White);
                }
            }

            public bool VerboseActive()
            {
                return _flags.Contains("v");
            }
        }
    }
}
